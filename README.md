# README #

dataGenerator für Masterthesis von Johannes Mayer

### What is this repository for? ###

Das Programm generiert große Datensätze, als komprimierte TSV-Dateien. 
Wie die Ausprägungen der Attribute des Datensatzes gebildet werden, ist konfigurierbar. Es steht zur Verfügung:

1. Vorgabe einer Liste mit Werten, aus denen zufällig Werte ausgewählt werden (gleichverteilt)
2. Vorgabe einer Liste mit Werten und zugehöriger relativer Häufigkeit
3. Zufällige Zeichenkette (Anzahl an distinct Values ist konfigurierbar

Für jedes Attribut kann die relative Häufigkeit von NULL-Werten angegeben werden. Für jeden Tag ab dem Startdatum wird eine Datei erzeugt.


### How do I get set up? ###

1. Maven und Git installieren.
2. Dieses Repository clonen
3. Konfigurationen im Programm vornehmen (Anzahl der Zeilen, Anzahl und Art der Spalten, Häufigkeit von NULL-Werten pro Spalte)
4. Mit maven kompilieren (Maven goal assembly:single)
5. Ausführen (benötigt in der aktuellen Konfiguration 6Gb Heap space)
    java jar -Xmx6g 100 2017-10-01 clickstream_data
	Parameter: Anzahl der Dateien die erzeugt werden, Datum der ersten Datei, Output-Ordner

