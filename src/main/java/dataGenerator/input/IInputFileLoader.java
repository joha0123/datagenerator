package dataGenerator.input;

import java.util.TreeMap;

public interface IInputFileLoader {
	public TreeMap<Double, String> getLinesMap();
}
