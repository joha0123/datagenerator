package dataGenerator.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.TreeMap;

public abstract class AbstractInputFileLoader implements IInputFileLoader {

	//protected FileReader fileReader;
	protected InputStream inputStream;
	protected BufferedReader bufferedReader;
	protected TreeMap<Double, String> linesMap;
	protected int nrOfCols;
	
	public AbstractInputFileLoader(String inputFileName) {
		this.linesMap = new TreeMap<Double, String>();
		
		try {
			this.inputStream = this.getClass().getClassLoader().getResourceAsStream(inputFileName);
			if (inputStream == null) {
				throw new IOException("File not found: \"" + inputFileName + "\"" );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		//fileReader = new FileReader(inputFilePath);
	    this.bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	    
		this.load();
		this.close();
	}
	
	protected abstract void load();
	
	private void close() {
        try {
			bufferedReader.close();
			inputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Getter for linesMap
	public TreeMap<Double, String> getLinesMap() {
		return this.linesMap;
	}
	
	// Getter for nrOfCols
	public int getNrOfCols() {
		return this.nrOfCols;
	}

}
