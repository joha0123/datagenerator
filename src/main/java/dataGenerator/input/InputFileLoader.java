package dataGenerator.input;

import java.io.IOException;

import dataGenerator.Configuration;

public class InputFileLoader extends AbstractInputFileLoader {

	public InputFileLoader(String inputFileName) {
		super(inputFileName);
	}

	@Override
	public void load() {
		String line = null;
		String [] splits = null;
		double counter = 0.0;
		try {
			while ((line = bufferedReader.readLine()) != null) {
				if (!line.equals("")){
					
					//split line on input delimiter
					splits = line.split("\t");
					String splittedLine = String.join(Configuration.getOutputDelimiter(), splits);

					// add to linesMap
					linesMap.put(counter++, splittedLine);

				}				
			}
			
			//take last split to determine the nr of columns
			this.nrOfCols = splits.length;
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
