package dataGenerator.input;

import java.io.IOException;
import java.util.Arrays;

import dataGenerator.Configuration;

public class InputFileLoaderWithFreq extends AbstractInputFileLoader {
	
	public InputFileLoaderWithFreq(String inputFileName) {
		super(inputFileName);
	}

	@Override
	protected void load() {
		String line = null;
		String [] splits = null;
		String [] temp = null;
		double cumulativeFreq = 0.0;

		try {
			while ((line = bufferedReader.readLine()) != null) {				
				
				if (!line.equals("")){ //because my computer is stupid and produces empty lines
					
					//split line on input delimiter, -1 keeps trailing spaces, so empty fields at the end of the line dont get discarted
					splits = line.split("\t", -1);	

					//restore original frequency on data creation frequency was multiplied by 100000
					cumulativeFreq += Double.parseDouble(splits[0]) / 100000;

					//remove first item and join the rest
					temp = Arrays.copyOfRange(splits, 1, splits.length);
					String strippedLine = String.join(Configuration.getOutputDelimiter(), temp);

					// add to linesMap
					linesMap.put(cumulativeFreq, strippedLine);

				}
			}
			
			//take last split to determine the nr of columns
			this.nrOfCols = temp.length;
			
		} catch (IOException | NullPointerException | NumberFormatException e) {
			e.printStackTrace();
		}
	}

}
