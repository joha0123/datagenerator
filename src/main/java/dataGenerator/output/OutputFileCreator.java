package dataGenerator.output;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.zip.GZIPOutputStream;

import dataGenerator.processing.DataRow;

public class OutputFileCreator {
	
	private FileOutputStream fileOutputStream;
	private Writer writer;

	public OutputFileCreator(String outputFilePath) {
		try {
			fileOutputStream = new FileOutputStream(outputFilePath);
			GZIPOutputStream gzipOut = new GZIPOutputStream(fileOutputStream, 65536, true);
			this.writer = new OutputStreamWriter(gzipOut, "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void write (DataRow row) {
		try {
			this.writer.write(row.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void close () {
		try {
			this.writer.flush();
			this.writer.close();
			this.fileOutputStream.flush();
			this.fileOutputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	

}
