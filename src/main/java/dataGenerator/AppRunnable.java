package dataGenerator;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import dataGenerator.input.InputFileLoaderWithFreq;
import dataGenerator.output.OutputFileCreator;
import dataGenerator.processing.DataRow;
import dataGenerator.processing.IColumn;
import dataGenerator.processing.columns.ColumnsFromFileWithFreq;
import dataGenerator.processing.columns.IdColumn;
import dataGenerator.processing.columns.TimestampColumn;
import dataGenerator.processing.columns.YNUColumn;
import dataGenerator.processing.columns.url.ClickActionUrlColumns;
import dataGenerator.processing.columns.url.ReferrerUrlColumns;


public class AppRunnable implements Runnable {
	
	final private String FILEPATH;
	final private int NROWS;
	private List<IColumn> randomColumns;
	private Random rand;
	private LocalDate date;


	public AppRunnable(String filePath, LocalDate date, int nRows, List<IColumn> randomColumns, Random rand) {
		this.FILEPATH = filePath + "/adobe_clickstream_" + date + ".tsv.gz";
		this.NROWS = nRows;
		this.randomColumns = randomColumns;
		this.rand = rand;
		this.date = date;
	}

	public void run() {
		
		List<IColumn> allColumns = new LinkedList<IColumn>();
		// Add columns from files
		allColumns.add(new ColumnsFromFileWithFreq(new InputFileLoaderWithFreq("ADB_COLUMNS_ACCEPT_LANGUAGE.tsv"), 0.01633f, rand));
		allColumns.add(new ColumnsFromFileWithFreq(new InputFileLoaderWithFreq("ADB_COLUMNS_BROWSER.tsv"), 0.0f, rand));
		allColumns.add(new ColumnsFromFileWithFreq(new InputFileLoaderWithFreq("ADB_COLUMNS_BROWSER_SIZE.tsv"), 0.00001f, rand));
		allColumns.add(new ColumnsFromFileWithFreq(new InputFileLoaderWithFreq("ADB_COLUMNS_CONNECTION.tsv"), 0.001f, rand));
		allColumns.add(new ColumnsFromFileWithFreq(new InputFileLoaderWithFreq("ADB_COLUMNS_GEO.tsv"), 0.008f, rand));
		allColumns.add(new ColumnsFromFileWithFreq(new InputFileLoaderWithFreq("ADB_COLUMNS_JS.tsv"), 0.114f, rand));
		allColumns.add(new ColumnsFromFileWithFreq(new InputFileLoaderWithFreq("ADB_COLUMNS_LANGUAGE.tsv"), 0.02f, rand));
		allColumns.add(new ColumnsFromFileWithFreq(new InputFileLoaderWithFreq("ADB_COLUMNS_OS.tsv"), 0.0062f, rand));
		allColumns.add(new ColumnsFromFileWithFreq(new InputFileLoaderWithFreq("ADB_COLUMNS_PAGE_EVENT.tsv"), 0.0f, rand));
		allColumns.add(new ColumnsFromFileWithFreq(new InputFileLoaderWithFreq("ADB_COLUMNS_PLUGIN.tsv"), 0.0f, rand));
		allColumns.add(new ClickActionUrlColumns(0.001f, 5000, rand));
		allColumns.add(new ReferrerUrlColumns(0.01f, 100000, rand));
		allColumns.add(new IdColumn(0.00001f, rand));
		allColumns.add(new TimestampColumn(0.001f, date, rand));
		allColumns.add(new IdColumn(0.00001f, rand));
		allColumns.add(new TimestampColumn(0.001f, date, rand));
		allColumns.add(new YNUColumn(0f, rand));

		// add the random columns
		allColumns.addAll(randomColumns);
		
		
		// Get total Number of columns
		int totalNrOfCols = 0;
		for (IColumn iColumn : allColumns) {
			totalNrOfCols += iColumn.getNrOfCols();
		}

		
		DataRow dr = new DataRow();
		OutputFileCreator of = new OutputFileCreator(this.FILEPATH);
		
		for (int i = 0; i < NROWS; i++) {
			
			dr.clear();
			for (IColumn col : allColumns) {

				dr.add(col.next());				
			}
			
			// DEBUGGING: Test if nrOfCols is correct
			if (dr.countCols() != totalNrOfCols){
				System.out.println("WARNING: Number of columns is incorrect!");
			}			
			
			of.write(dr);

		}
		


		System.out.println("File \"" + FILEPATH + "\"" + " created! " + totalNrOfCols + " columns");
		of.close();


	}

}
