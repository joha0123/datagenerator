package dataGenerator;

import java.io.File;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.List;

import dataGenerator.input.InputFileLoader;
import dataGenerator.processing.columns.RandomColumnsFromStatsFile;
import dataGenerator.processing.IColumn;


public class Main {
	
	public static void main(String[] args) {
		
		final int NROFFILES = Integer.valueOf(args[0]);
		final String INPUTDATE = args[1];		
		String basePath = "clickstream";
		
		//third parameter is optional
		if (args.length == 3){
			basePath = args[2];
		}
		
		Thread [] threads = new Thread[NROFFILES];
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate date = LocalDate.parse(INPUTDATE, formatter);
		
		System.out.println("Generating random columns ...");
		
		//Create Random Columns
		Random rand = new Random();
		Random fixedRand = new Random(123214123);
		RandomColumnsFromStatsFile rcff = new RandomColumnsFromStatsFile(new InputFileLoader("ADB_STATS.tsv"), fixedRand, rand);
		List<IColumn> randomColumns = rcff.getAll();
		
		
		//create threads
		for (int i = 0; i < threads.length; i++) {
			
			//create between 80000 and 130000 rows per file
			int nRows = rand.nextInt(30000) + 60000;
			
			//create directory
			DecimalFormat dFormat = new DecimalFormat("00");			
			String year = dFormat.format(Double.valueOf(date.getYear()));
			String month = dFormat.format(Double.valueOf(date.getMonthValue()));
			String day = dFormat.format(Double.valueOf(date.getDayOfMonth()));
			String filePath = basePath + "/ds=" + year + "-" + month + "-" + day;
			File dir = new File(filePath);
			dir.mkdirs(); //creates all directories that don't exist in the path			
			
			threads[i] = new Thread(new AppRunnable(filePath, date, nRows, randomColumns, rand));
			
			date = date.plusDays(1);
			
		}
		
		System.out.println("Creating files ...");
		
		ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		for (Thread thread : threads) {
			exec.submit(thread);
		}
		
		exec.shutdown();
	}

}
