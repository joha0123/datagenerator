package dataGenerator.processing;

import dataGenerator.Configuration;

public class DataRow {

	StringBuilder sb;
	String delimiter;
	
	public DataRow() {
		this.sb = new StringBuilder();
		this.delimiter = Configuration.getOutputDelimiter();
	}
	
	public void add(String text) {
		sb.append(text).append(delimiter);
	}
	
	public void clear () {
		sb.setLength(0);
	}
	
	public int countCols() {
		return sb.substring(0, sb.length() - delimiter.length()).split(Configuration.getOutputDelimiter(), -1).length;
	}
	
	@Override
	public String toString() {		
		//Cut off last delimiter and append newline command
		return sb.substring(0, sb.length() - delimiter.length()) + Configuration.getEndOfLine();
	}

}
