package dataGenerator.processing;

import java.util.Random;

import dataGenerator.Configuration;

public abstract class AbstractColumn implements IColumn{
	
	protected Random rand;
	protected float nullDistribution;
	protected int nrOfCols;

	public AbstractColumn(float nullDistribution, int nrOfCols, Random rand) throws IllegalArgumentException {
		
		// Only allow values between 0 and 1 meaning the percentage of NULL values in this column
		// 0: No NULL values in this column, 1: every value is NULL 
		if(nullDistribution < 0f || nullDistribution > 1f) {
			throw new IllegalArgumentException("Only values between 0 and 1 are allowed");
		}
		
		this.nullDistribution = nullDistribution;
		this.rand = rand;
		this.nrOfCols = nrOfCols;

	}
	
	protected String valueOrNull(String value) {
		String back = value;
		
		if (nextIsNull()) {
			back = this.generateNullLine();
		}
		
		return back;
	}
	
	// determines if the next value to return should be null or not null
	private Boolean nextIsNull() {
		
		//returns value between 0.0 and 1.0
		float chance = rand.nextFloat();
		Boolean back = true;

		if (chance > this.nullDistribution) {
			back = false;
		}
		
		return back;
	}
	
	// returns the correct number of null values with delimiters for a given number of columns (this.nrOfCols)
	private String generateNullLine () {
		String nullValue = Configuration.getNullValue();
		String delimiter = Configuration.getOutputDelimiter();
		StringBuilder sb = new StringBuilder();
		
		for (int i = 0; i < this.nrOfCols; i++) {
			sb.append(nullValue).append(delimiter);
		}
		
		return sb.substring(0, sb.length() - delimiter.length());
	}
	
	public int getNrOfCols() {
		return this.nrOfCols;
	}
	
}
