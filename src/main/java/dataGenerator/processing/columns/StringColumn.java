package dataGenerator.processing.columns;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import dataGenerator.Configuration;
import dataGenerator.processing.AbstractColumn;

public class StringColumn extends AbstractColumn {

	private Random fixedRand;
	private String[] stringPool;
	private char[] charPool;
	private int nrDistinct;
	private int variableLength;
	private int minLength;
	private String salt;

	public StringColumn(float nullDistribution, int nrDistinct, Random rand, Random fixedRandom)
			throws IllegalArgumentException {
		super(nullDistribution, 1, rand);

		// Seed random for Stringpoolgeneration, so all Threads generate the
		// same Stringpool
		this.fixedRand = fixedRandom;
		this.stringPool = new String[nrDistinct];
		this.charPool = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
		this.minLength = 10;
		this.variableLength = 10;
		this.nrDistinct = nrDistinct;
		this.salt = "abeabeadbbeaaebeabeabeaadbeaabeaadbeaa"; //salt is used to achieve good compression
		this.generateStringpool();

	}

	@Override
	public String next() {
		String value = Configuration.getNullValue();

		if (stringPool.length > 0) {
			value = stringPool[rand.nextInt(stringPool.length)];
		}

		return valueOrNull(value + salt);
	}

	private void generateStringpool() {

		Set<String> tempSet = new HashSet<String>();

		while (tempSet.size() < this.nrDistinct) {

			// Put salted randomstring into tempSet
			tempSet.add(generateRandomString(fixedRand.nextInt(variableLength) + minLength));
		}

		//convert to array
		this.stringPool = tempSet.toArray(new String[tempSet.size()]);
	}

	private String generateRandomString(int length) {
		char buf[] = new char[length];
		for (int i = 0; i < buf.length; i++) {
			int index = fixedRand.nextInt(charPool.length);
			buf[i] = charPool[index];
		}
		
		return new String(buf);
	}

}
