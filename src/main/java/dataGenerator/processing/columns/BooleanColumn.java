package dataGenerator.processing.columns;

import java.util.Random;

import dataGenerator.processing.AbstractColumn;

public class BooleanColumn extends AbstractColumn {
	
	//Return boolean as text (true | false) or as number (1 | 0)
	Boolean numeric;
	
	public BooleanColumn(Boolean numeric, float nullDistribution, Random rand) throws IllegalArgumentException {
		this(nullDistribution, rand);
		this.numeric = numeric;
	}

	public BooleanColumn(float nullDistribution, Random rand) throws IllegalArgumentException {
		super(nullDistribution, 1, rand);
		
		// Default value for numeric
		this.numeric = false;
	}

	public String next() {
		String value = null;

		Boolean randomBool = rand.nextBoolean();			
		if (numeric) {
			value = String.valueOf(randomBool ? 1 : 0);
		}
		else {
			value = String.valueOf(randomBool);
		}

		return valueOrNull(value);
	}

}
