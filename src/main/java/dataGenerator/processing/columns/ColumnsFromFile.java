package dataGenerator.processing.columns;

import java.util.Random;
import java.util.TreeMap;
import dataGenerator.input.InputFileLoader;
import dataGenerator.processing.AbstractColumn;

public class ColumnsFromFile extends AbstractColumn {

	private TreeMap<Double, String> inputLinesMap;
	private Double[] keysArray;

	public ColumnsFromFile(InputFileLoader inputFileLoader, float nullDistribution, Random rand)
			throws IllegalArgumentException {
		super(nullDistribution, inputFileLoader.getNrOfCols(), rand);

		this.inputLinesMap = inputFileLoader.getLinesMap();
		// create Array with Keys inside
		this.keysArray = inputLinesMap.keySet().toArray(new Double[inputLinesMap.keySet().size()]);
	}

	public String next() {		
		// get random record from list
		String value = this.inputLinesMap.get(keysArray[rand.nextInt(keysArray.length)]);

		return valueOrNull(value);
	}

}
