package dataGenerator.processing.columns;

import java.util.Random;

import dataGenerator.processing.AbstractColumn;

public class NullColumns extends AbstractColumn {

	public NullColumns(int nrOfCols, Random rand) throws IllegalArgumentException {
		super(1.0f, nrOfCols, rand);

	}

	@Override
	public String next() {
		return valueOrNull(null);
	}

}
