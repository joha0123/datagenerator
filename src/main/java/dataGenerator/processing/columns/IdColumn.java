package dataGenerator.processing.columns;

import java.util.Random;
import java.util.UUID;

import dataGenerator.processing.AbstractColumn;

public class IdColumn extends AbstractColumn {

	public IdColumn(float nullDistribution, Random rand) throws IllegalArgumentException {
		super(nullDistribution, 1, rand);
	}

	@Override
	public String next() {
		String value = UUID.randomUUID().toString();
		return valueOrNull(value);
	}

}
