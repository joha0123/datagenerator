package dataGenerator.processing.columns;

import java.util.ArrayList;
import java.util.Random;

import dataGenerator.input.InputFileLoader;
import dataGenerator.processing.IColumn;

public class RandomColumnsFromStatsFile {
	
	private ArrayList<IColumn> randomColumnsList;
	private ArrayList<String> linesList;
	private int nrOfColumns;
	private Random rand;
	private Random fixedRand;
	private float overallSourceRows;
	
	public RandomColumnsFromStatsFile(InputFileLoader inputFile, Random fixedRand, Random rand) {

		this.linesList = new ArrayList<String>(inputFile.getLinesMap().values());
		this.nrOfColumns = linesList.size();
		this.randomColumnsList = new ArrayList<IColumn>(nrOfColumns);
		this.rand = rand;
		this.fixedRand = fixedRand;
		
		this.overallSourceRows = 35228572.0f;
		
		this.generateColumns();
	}
	
	public ArrayList<IColumn> getAll() {
		return this.randomColumnsList;
	}
	
	private void generateColumns() {

		for (int i = 0; i < this.nrOfColumns; i++) {
			String [] values = linesList.get(i).split("\t");
			float nullDistribution = -1.0f;
			int nrDistinct = -1;
			
			try {
				
				nullDistribution = Float.parseFloat(values[1]) / overallSourceRows;
				nrDistinct = Integer.parseInt(values[0]);				
				
			} catch (NullPointerException | NumberFormatException e) {
				e.printStackTrace();
			}
//			System.out.println("distinct: " + nrDistinct);
//			System.out.println("nullD: " + nullDistribution);
			
//			if (nullDistribution < 0 || nullDistribution > 1) {
//				System.out.println("error");
//			}
//			
//			if (nrDistinct < 0 || nrDistinct > (int) 35228572.0f)
//			{
//				System.out.println("error dist");
//			}
			
			//The last argument fixedRandom generates a seeded Random for each column in each thread
			randomColumnsList.add(new StringColumn(nullDistribution, nrDistinct, rand, fixedRand));
		}
	}

}
