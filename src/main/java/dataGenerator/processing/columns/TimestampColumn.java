package dataGenerator.processing.columns;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Random;

import dataGenerator.processing.AbstractColumn;

public class TimestampColumn extends AbstractColumn {
	
	private long startTimestamp;

	public TimestampColumn(float nullDistribution, LocalDate date, Random rand) throws IllegalArgumentException {
		super(nullDistribution, 1, rand);
		this.startTimestamp = Timestamp.valueOf(date.atStartOfDay()).toInstant().getEpochSecond();
	}

	@Override
	public String next() {
		long value = startTimestamp + rand.nextInt(60 * 60 * 24);
		return valueOrNull(Long.toString(value));
	}
	
}
