package dataGenerator.processing.columns.url;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.RandomStringUtils;

import dataGenerator.Configuration;

public class ReferrerUrlColumns extends AbstractUrlColumns {

	public ReferrerUrlColumns(float nullDistribution, int nrUniqueSites, Random rand) throws IllegalArgumentException {
		super(nullDistribution, 2, rand);
		
		this.domains = new String [] {"google", "yahoo", "msn", "indeed", "stepstone",	"bing", "monster", "web"};
		this.assemblyColumns(nrUniqueSites);
	}


	@Override
	protected void assemblyColumns(int nrUniqueRows) {
		for (int i = 0; i < nrUniqueRows; i++) {
			StringBuilder sb = new StringBuilder();
			String url = this.generateUrl();
			
			sb.append(url);
			sb.append(Configuration.getOutputDelimiter());
			
			sb.append(this.extractRefName(url));
			
			this.urlColsList.add(sb.toString());
		}
	}
	
	private String generateUrl() {
		StringBuilder sb = new StringBuilder();
		String protocol = this.protocols[rand.nextInt(this.protocols.length)];
		String subdomain = this.subdomains[rand.nextInt(this.subdomains.length)];
		String domain = this.domains[rand.nextInt(this.domains.length)];
		String toplevelDomain = this.toplevelDomains[rand.nextInt(this.toplevelDomains.length)];
		
		sb.append(protocol).append("://").append(subdomain).append(".").append(domain).append(".").append(toplevelDomain).append("/");
		sb.append(RandomStringUtils.randomAlphabetic(rand.nextInt(10) + 1).toLowerCase()).append("/");
		sb.append(RandomStringUtils.randomAlphanumeric(rand.nextInt(15) + 1).toLowerCase());
		
		return sb.toString();
	}
	
	private String extractRefName(String fullUrl) {
		Pattern p = Pattern.compile(".+\\.(.*)\\.(.{2}).*");
		Matcher m = p.matcher(fullUrl);
		String back = "";
		
		if (m.find()) {
			String name = m.group(1);
			String country = m.group(2);
			
			back = name.substring(0, 1).toUpperCase() + name.substring(1) + " - " + country.toUpperCase();
		}

		return back;
	}
	

}
