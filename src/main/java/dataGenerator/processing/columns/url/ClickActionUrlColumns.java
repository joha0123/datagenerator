package dataGenerator.processing.columns.url;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.lang.RandomStringUtils;

import dataGenerator.Configuration;

public class ClickActionUrlColumns extends AbstractUrlColumns {

	private String[] level1;
	private String[] level2;
	private Map<Integer, String[]> sites;

	public ClickActionUrlColumns(float nullDistribution, int nrUniqueSites, Random rand)
			throws IllegalArgumentException {
		super(nullDistribution, 6, rand);
		this.domains = new String[] { "example" };
		this.level1 = new String[] { "cms", "dkms", "svtl" };
		this.level2 = new String[] { "products", "applications", "solutions", "info", "tools", "career" };

		// Generate a pool of X unique Sites
		this.generateSites(nrUniqueSites);

		this.assemblyColumns(nrUniqueSites);
	}

	// If column is added, nrOfCols has to be adjusted in super constructor!
	@Override
	protected void assemblyColumns(int nrUniqueRows) {
		for (int i = 0; i < nrUniqueRows; i++) {
			String[] clickAction = this.sites.get((Integer) rand.nextInt(sites.size()));
			StringBuilder sb = new StringBuilder();

			sb.append(clickAction[0]);
			sb.append(Configuration.getOutputDelimiter());

			sb.append(clickAction[1]);
			sb.append(Configuration.getOutputDelimiter());

			sb.append(this.extractAfterLangSlash(clickAction[0]));
			sb.append(Configuration.getOutputDelimiter());

			sb.append(this.extractCategory(clickAction[0]));
			sb.append(Configuration.getOutputDelimiter());

			String[] startPage = this.sites.get((Integer) rand.nextInt(sites.size()));
			sb.append(startPage[0]);
			sb.append(Configuration.getOutputDelimiter());

			sb.append(this.extractAfterLangSlash(clickAction[0]));

			this.urlColsList.add(sb.toString());
		}
	}

	private void generateSites(int nrOfUniqueSites) {
		this.sites = new HashMap<Integer, String[]>();
		int key = 0;
		for (int i = 0; i < nrOfUniqueSites; i++) {
			String[] value = new String[2];
			value[0] = this.generateClickAction();
			value[1] = this.generateId();
			this.sites.put((Integer) key++, value);
		}
	}
	

	private String generateClickAction() {
		StringBuilder sb = new StringBuilder();
		String protocol = this.protocols[rand.nextInt(this.protocols.length)];
		String subdomain = this.subdomains[rand.nextInt(this.subdomains.length)];
		String domain = this.domains[rand.nextInt(this.domains.length)];
		String toplevelDomain = this.toplevelDomains[rand.nextInt(this.toplevelDomains.length)];
		String l1 = this.level1[rand.nextInt(this.level1.length)];
		String lang = toplevelDomain;
		String l2 = this.level2[rand.nextInt(this.level2.length)];

		sb.append(protocol).append("://").append(subdomain).append(".").append(domain).append(".")
				.append(toplevelDomain).append("/").append(l1).append("/").append(lang).append("/").append(l2)
				.append("/");
		sb.append(RandomStringUtils.randomAlphabetic(rand.nextInt(10) + 1).toLowerCase());
		sb.append("/").append(RandomStringUtils.randomAlphanumeric(rand.nextInt(15) + 1).toLowerCase());
		sb.append("/").append(RandomStringUtils.randomAlphanumeric(rand.nextInt(20) + 1).toLowerCase());
		return sb.toString();
	}
	

	// Generates an Id like: 7b5352b7-fa9edd3c-96da61a0-7b270728-5ab8188
	private String generateId() {
		return UUID.randomUUID().toString();
	}
	

	private String extractAfterLangSlash(String fullUrl) {
		String[] splits = fullUrl.split("/");
		String back = "";

		for (int i = 4; i < splits.length; i++) {
			back += splits[i] + "/";
		}

		return back.substring(0, back.length() - 1);
	}
	

	private String extractCategory(String fullUrl) {
		String[] splits = fullUrl.split("/");

		return splits[5] + "[" + splits[4] + "]";
	}

}
