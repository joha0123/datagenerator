package dataGenerator.processing.columns.url;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import dataGenerator.processing.AbstractColumn;

public abstract class AbstractUrlColumns extends AbstractColumn {
	
	protected String [] protocols;
	protected String [] subdomains;
	protected String [] domains;
	protected String [] randomStrings;
	protected String [] toplevelDomains;
	protected List<String> urlColsList;
	
	
	public AbstractUrlColumns(float nullDistribution, int nrOfCols, Random rand) throws IllegalArgumentException {
		super(nullDistribution, nrOfCols, rand);
		
		this.urlColsList = new ArrayList<String>();
		this.protocols = new String [] {"http", "https"};
		this.subdomains = new String [] {"www"};
		this.toplevelDomains = new String [] {"com", "org", "uk", "net", "ca", "de", "jp", "fr", "au", "us", "ru", "ch", "it", "nl", "se", "no", "es"};
	}

	public String next() {
		return this.valueOrNull(urlColsList.get(rand.nextInt(urlColsList.size())));
	}
	
	abstract void assemblyColumns(int nrUniqueRows);
	
	


}
