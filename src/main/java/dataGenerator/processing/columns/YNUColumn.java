package dataGenerator.processing.columns;

import java.util.Random;

import dataGenerator.processing.AbstractColumn;

public class YNUColumn extends AbstractColumn {
	
	String [] values;

	public YNUColumn(float nullDistribution, Random rand) throws IllegalArgumentException {
		super(nullDistribution, 1, rand);
		
		this.values = new String [] {"Y", "N", "U"};
	}

	public String next() {
		String value = values[rand.nextInt(values.length)];
		
		return valueOrNull(value);
	}

}
