package dataGenerator.processing.columns;

import java.util.Random;
import java.util.TreeMap;
import dataGenerator.input.InputFileLoaderWithFreq;
import dataGenerator.processing.AbstractColumn;

public class ColumnsFromFileWithFreq extends AbstractColumn {

	private TreeMap<Double, String> linesMap;
	
	public ColumnsFromFileWithFreq(InputFileLoaderWithFreq inputFileLoader, float nullDistribution, Random rand) throws IllegalArgumentException {
		super(nullDistribution, inputFileLoader.getNrOfCols(), rand);
		
		this.linesMap = inputFileLoader.getLinesMap();

	}

	public String next() {
		//randomDouble is between 0 (inclusive) and the highest map-key value (exclusive) 
		double randomDouble = rand.nextDouble() * linesMap.lastKey();
		String value = this.linesMap.higherEntry(randomDouble).getValue();

		return valueOrNull(value);
	}

}
