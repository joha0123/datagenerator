package dataGenerator.processing.columns;

import java.util.Random;

import dataGenerator.processing.AbstractColumn;

public class NumberColumn extends AbstractColumn {
	
	int maximum;

	public NumberColumn(int maximum, float nullDistribution, Random rand) throws IllegalArgumentException {
		super(nullDistribution, 1, rand);
		
		this.maximum = maximum;
		
	}

	public String next() {
		String value = String.valueOf(rand.nextInt(maximum));		
		
		return valueOrNull(value);
	}

}
