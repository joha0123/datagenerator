package dataGenerator.processing;

public interface IColumn {
	public String next();
	public int getNrOfCols();
}
