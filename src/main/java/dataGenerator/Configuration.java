package dataGenerator;

public final class Configuration {

	public static String getNullValue() {
		return "";
	}
	
	public static String getOutputDelimiter() {
		return "\t";
	}
	
	public static String getEndOfLine() {		
		return "\n";
	}

}
